import re
class CustomPreprocessor():
    def __init__(self):
        self._means = None
    

    def replace_phone_number(self,text):
        '''
        Regex pattern to replace phone number with MOBILE_NUMBER
        Objective is to standardize all the phone number patterns
        '''
        find_all_mobile_number = re.compile(r'(?:(?:\+|0{0,2})91(\s*[\ -]\s*)?|[0]?)?[789]\d{9}|(\d[ -]?){10}\d$')
        return re.sub(pattern= find_all_mobile_number, repl='MOBILE_NUMBER', string=text)


    def replace_user_mention(self,text):
        '''
        Regex pattern to replace usermention with USER_MENTION
        Objective is to standardize all the usermention patterns
        '''
        find_all_user_mention = re.compile(r'@\w*')
        return re.sub(pattern= find_all_user_mention, repl='USER_MENTION', string=text)


    def replace_url(self,text):
        '''
        Regex pattern to replace urls with URL
        Objective is to standardize all the URL patterns
        '''
        find_all_url = re.compile(
            r'(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))'
            r'[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})')
        return re.sub(pattern= find_all_url, repl='URL', string=text)


    def replace_class_mention(self,text):
        '''
        Regex pattern to replace class mention with CLASS_TH
        Objective is to standardize all the class mention  patterns
        Example: 10th , 1st -> to class mention
        '''

        find_all_class = re.compile(r'[0-9]+(?:st|[nr]d|th)')
        return re.sub(pattern= find_all_class, repl='CLASS_TH', string=text)


    def replace_salary_mention(self,text):
        '''
        Regex pattern to replace class mention with CLASS_TH
        Objective is to standardize all the class mention  patterns
        Example: 10k,50k -> to salary mention
        '''

        find_salary_mention = re.compile(r'[0-9]+(?:k)')
        return re.sub(pattern= find_salary_mention, repl='SALARY_MENTION', string=text)


    def replace_emojis_pattern(self,text):
        '''
        Regex pattern to remove emojis
        '''

        try:
            emojis_pattern = re.compile(u'([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF])')
        except re.error:
            emojis_pattern = re.compile(
                u'([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF])')
        return re.sub(pattern= emojis_pattern, repl=' ', string=text)


    def preprocess_text(self,text):
        """
        Cleaning the text using above functions
        """
        text=text.replace('\n',' ')
        text=text.replace('*','')
        text=text.replace('/',' ')
        text = text.lower()
        text = self.replace_phone_number(text)
        text = self.replace_class_mention(text)
        text = self.replace_url(text)
        text = self.replace_user_mention(text) 
        text = self.replace_salary_mention(text)
        text = self.replace_emojis_pattern(text)

        return text
    
    def preprocess_data(self,data):
        try:
            return data['text'].astype(str).apply(self.preprocess_text)
        except:
            return [self.preprocess_text(i) for i in data]
        

# docker push asia-south1-docker.pkg.dev/apnatime-fbc72/jobseekcommunity/vertex-pred
# gcloud auth configure-docker asia-south1-docker.pkg.dev