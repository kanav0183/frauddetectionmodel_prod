import pickle
import joblib
import numpy as np
import pandas as pd
from fastapi import FastAPI
from typing import List, Optional


##########################  CONFIG  ###############################################
AIP_PREDICT_ROUTE='/predict'
AIP_HEALTH_ROUTE ='/health'
MODEL_DESCRIPTION ="Generic vs Non Generic model"
EXTRA_FEATURES = [  'char_count','word_count','is_reply','time_difference',
                    'is_mob_num', 'is_class', 'is_user_mention',
                    'is_salary', 'is_url','friends_count','referral_count',
                    'view_count','is_verified']
###########################################################################

################################ UTILS ###########################################
from typing import List
from pydantic import BaseModel

class PostFeature(BaseModel):
    text: str
    time_difference: int
    friends_count: int
    referral_count: int
    view_count : int
    is_verified : int
    is_reply: int
    
class Parameters(BaseModel):
    return_confidence: bool
    
class Prediction(BaseModel):
    confidence: float
    model_description: str

class Predictions(BaseModel):
    predictions: List[Prediction]
    
###########################################################################




model = joblib.load('model_files/model.joblib')
vectorizer = joblib.load('model_files/vector.joblib')
with open('model_files/preprocessor.pkl', 'rb') as f:
    preprocessor = pickle.load(f)

app = FastAPI()

@app.get(AIP_HEALTH_ROUTE, status_code=200)
async def health():
    return {'health': 'ok'}

@app.post(AIP_PREDICT_ROUTE,
          response_model=Predictions,
          response_model_exclude_unset=True)
async def predict(instances: List[PostFeature],
                  parameters: Optional[Parameters] = None):

    instances = pd.DataFrame([x.dict() for x in instances])
    instances['char_count'] = instances.text.apply(len)
    instances['word_count'] = instances.text.apply(lambda x: len(x.split(' ')))
    instances['char_count'] = instances.text.apply(len)
    instances['cleaned_text'] = instances['text'].astype(str).apply(preprocessor.preprocess_text)
    instances['is_mob_num']=instances.cleaned_text.apply(lambda x: "MOBILE_NUMBER" in x).astype(int)
    instances['is_class']=instances.cleaned_text.apply(lambda x: "CLASS_TH" in x).astype(int)
    instances['is_user_mention']=instances.cleaned_text.apply(lambda x: "USER_MENTION" in x).astype(int)
    instances['is_salary']=instances.cleaned_text.apply(lambda x: "SALARY_MENTION" in x).astype(int)
    instances['is_url']=instances.cleaned_text.apply(lambda x: "URL" in x).astype(int)
    preprocessed_inputs_arr = vectorizer.transform(instances.cleaned_text).toarray()
    preprocessed_inputs = np.concatenate([preprocessed_inputs_arr,instances[EXTRA_FEATURES].values ],axis=1)
    preds = model.predict_proba(preprocessed_inputs)[:,1]
    outputs = []
    for i in preds:
        outputs.append(Prediction(confidence = i,model_description =MODEL_DESCRIPTION ))
    return Predictions(predictions=outputs)